var webpack = require('webpack');
var path = require('path');

var fs = require('fs');
var dir = path.resolve(__dirname, 'public/js/');
var files = fs.readdirSync(dir);
var entries = {}; // multiple entries for chunking files
files.filter(function(filename){
  return /\.js$|\.jsx$/.exec(filename);
}).forEach(function(filename){
  entries[filename] = path.resolve(dir, filename);
});

var BUILD_DIR = path.resolve(__dirname, 'public/js/build');
var APP_DIR = path.resolve(__dirname, 'public/js/');

var config = {
  entry: entries,
  output: {
    path: BUILD_DIR,
    filename: '[name]'
  },
  module : {
    loaders : [
      {
        test : /\.js$|\.jsx$/,
        include : APP_DIR,
        loader : 'babel'
      }
    ]
  }
};

module.exports = config;

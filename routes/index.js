const keystone = require('keystone');
const middleware = require('./middleware');
const importRoutes = keystone.importer(__dirname);
const i18n = require('i18n');
const path = require('path');
const passport = require('passport');
const expressSession = require('express-session');
const cookieParser = require('cookie-parser')

var locales = ['en', 'zh-hk'];

i18n.configure({
		locales:locales,
		directory: __dirname + '/../locales',
		defaultLocale: locales[0]
});

keystone.pre('routes', i18n.init);

keystone.pre('routes', function (req, res, next) {
	res.locals.navLinks = [
		{ label: 'Home', key: 'home', href: '/' },
		{ label: 'Blog', key: 'blog', href: '/blog' },
		{ label: 'Gallery', key: 'gallery', href: '/gallery' },
		{ label: 'Contact', key: 'contact', href: '/contact' },
	];
	//console.log(i18n.getLocale());
	i18n.setLocale(req.cookies.locale || i18n.getLocale());
	res.locals.user = req.user;
	res.locals.lang = i18n.__('view');
	res.locals.i18nLocale = i18n.getLocale();
	res.locals.i18nCatalog = i18n.getLocales();
	keystone.list('Module').model.find().exec(function(err, modules){
		res.locals.modules = modules;
		next();
	});
});

keystone.pre('render', middleware.theme);
keystone.pre('render', middleware.flashMessages);

keystone.set('404', function (req, res, next) {
	res.status(404).render('errors/404');
});

// Load Routes
var routes = {
	download: importRoutes('./download'),
	views: importRoutes('./views'),
};


exports = module.exports = function (app) {

	const multerImpl = require('./multerImpl')(app);
	app.use(expressSession());
	app.use(passport.initialize());
	app.use(passport.session());
	app.use(cookieParser());
	app.post('/locale', function(req, res){
		res.cookie('locale', req.body.locale || 'en');
		res.send('hello world');
		res.end();
	})


	Object.keys(routes.views).forEach(function(v){
		if(typeof routes.views[v] === 'object'){
			Object.keys(routes.views[v]).forEach(function(f){
				if(typeof routes.views[v][f] === 'object'); // skip if there is multiple function exports in an middleware
				else{
					console.log('(routes): ', path.resolve('/',v,f));
					app.get(path.resolve('/',v,f), routes.views[v][f]);
				}
			});
		}else{
			console.log('(routes): ', path.resolve('/',v));
			app.get(path.resolve('/',v), routes.views[v]);
		}
	});

	app.get('/', routes.views.index);
	var fbMiddleware = routes.views.auth.facebook;
	app.get('/auth/facebook', fbMiddleware.authenticate);
	app.get('/auth/facebook/callback', fbMiddleware.authenticate, fbMiddleware.failureRedirect, fbMiddleware.successRedirect);
	//app.get('/auth/facebook', passport.authenticate('facebook'));
	/*app.get('/auth/facebook/callback',
  	passport.authenticate('facebook', {failureRedirect: '/login' }),
			function(req, res){
			},
			function(user,req,res,next) {
				console.log("----success facebook auth ----");
				console.log("----facebook user info: ----");
				if(!req.user){
					//signin(req,res,user.email,user.password);
					//console.log(user);
					res.redirect('/');
				}else{
					console.log("already signin");
					res.redirect('/');
				}
			}
	);*/

}

var keystone = require('keystone');
var async = require('async');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	res.locals.apiData = {};
	//res.locals.apiData = {project: 'hihi', title: 'hihi'};
	view.on('init', function(next){
		async.waterfall([

			function(callback){
				keystone.list("Education").model
				.find().exec(function(err, edu){
					res.locals.apiData.education = edu;
					callback(err);
				});
			},

			function(callback){
				async.forEachOf(res.locals.apiData.education, function(value, key, cb){
					keystone.list("Stage").model.find().where("education", value._id).exec(function(err, stages){
						// Mongoose json object is freeze, we need to copy one new object and replace it.
						res.locals.apiData.education[key] = res.locals.apiData.education[key].toObject();
						res.locals.apiData.education[key].stage = stages;
						cb(err);
					});
				}, function(err){
					callback(err);
				});
			},

			function(callback){
				async.forEachOf(res.locals.apiData.education, function(value, key, cb){
					keystone.list("School").model.find().where("education", value._id).exec(function(err, schools){
							res.locals.apiData.education[key].school = schools;
							cb(err);
					});
				}, function(err){
					callback(err);
				});

			}

		], function(err, result){
			next(err);
		});
	})

	view.render('upload');
}

const keystone = require('keystone');
const passport = require('passport');
const FacebookStrategy = require('passport-facebook').Strategy;
const bcrypt = require('bcrypt-nodejs');


function signin(req, res, email, password){
  keystone.session.signin(
    {
      email: email,
      password: password
    },
    req, res,
    function(user){
      // success signin
      console.log("req.user: "+req.user);
      res.redirect('/upload');
    },
    function(err){
      // failure signin
      console.log(err);
			res.redirect('/auth/login');
    }
  );
}

  passport.use(new FacebookStrategy({
      clientID: '1730853303822478',
      clientSecret: '1b8adcd78732cb0bcf70701240033578',
      callbackURL: "/auth/facebook/callback",
      profileFields: ['id', 'displayName', 'photos', 'email'],
      passReqToCallback: true     //- usage of this flag:  http://passportjs.org/docs#association-in-verify-callback
    },
    function(req, accessToken, refreshToken, profile, done) {
      console.log('---accessToken---');
      console.log(accessToken);
      console.log("---after auth, the user profile ---");
      console.log(profile);
      var User = keystone.list('User');
      User.model.findOne({facebookID: profile.id}, function (error, user) {
        if(!user){ // user not exist
          var newUser = new User.model({
            name: {first: '', last: profile.displayName},
            email: profile.email || profile.id+'@dummyemail.com',
            facebookID: profile.id,
            password: accessToken,
            isAdmin: false,
            isProtected: true,
            facebookIcon: profile.photos[0].value || ''
          });
          newUser.save(function(err, user){
            //signin(req,res,newUser.email, accessToken);
            req.session.accessToken = accessToken;
            done(user);
          });
        }else{
          user.name = {first: '', last: profile.displayName};
          user.password = accessToken;
          user.isProtected = false;
          user.facebookIcon = profile.photos[0].value || '';
          user.save(function(err, updateduser){
            console.log("----after updating password----");
            console.log(updateduser);
            //signin(req,res,user.email,accessToken);
            req.session.accessToken = accessToken;
            done(updateduser);
          })
        }
      });
    }
  ));

exports.authenticate = passport.authenticate('facebook');

exports.failureRedirect = function(req,res){};

exports.successRedirect = function(user,req,res,next) {
  console.log("----success facebook auth ----");
  console.log("----facebook user info: ----");
  if(!req.user){
    console.log("redirect user object: "+user);
    //console.log("can I get accessToken to signin: "+req.session.accessToken);
    signin(req,res,user.email,req.session.accessToken);
  }else{
    console.log("already signin");
    res.redirect('/');
  }
}

const keystone = require('keystone');

function signin(req, res, email, password){
  keystone.session.signin(
    {
      email: email,
      password: password
    },
    req, res,
    function(user){
      // success signin
      console.log("req.user: "+req.user);
      res.redirect('/');
    },
    function(err){
      // failure signin
      console.log(err);
			res.redirect('/auth/login/?failed');
    }
  );
}


exports = module.exports = function(req, res, next){
  signin(req,res,req.param('email'), req.param('password'))
}

var keystone = require('keystone');

exports = module.exports = function (req, res) {

  var view = new keystone.View(req, res);

  keystone.list("Education").model.find().exec().then(function(educations){
    res.locals.apiData = educations || {};
  	//console.log(res.locals);
  	view.render('auth/register', res.locals);
  })

}

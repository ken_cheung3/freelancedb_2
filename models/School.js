var keystone = require('keystone');
var Types = keystone.Field.Types;

var School = new keystone.List('School', {
  label: 'School'
});

School.add({
	value: { type: Number, required: true, index: true, unique: true, initial: true },
	name: { type: String, unique: false, index: false},
  education: { type: Types.Relationship, ref: 'Education', many: true, required: true, initial: true  },
  order: { type: Number, unique: false, index: false, initial: true, required: true}
});


School.track = true;
School.defaultSort = '+order';
School.defaultColumns = 'value, name, education, order';
School.register();

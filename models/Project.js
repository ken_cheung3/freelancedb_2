var keystone = require('keystone');
var Types = keystone.Field.Types;

var Project = new keystone.List('Project', {
	autokey: { from: 'name', path: 'key', unique: true },
});

Project.add({
	name: { type: String, required: true },
	author: { type: Types.Relationship, ref: 'User', index: true },
	publishedDate: { type: Types.Date, index: true },
	content: {
		brief: { type: Types.Html, wysiwyg: true, height: 150 },
		extended: { type: Types.Html, wysiwyg: true, height: 400 },
	},
  _document: {
    type: Types.S3File,
    filename: function(item, filename){
		  // prefix file name with object id
		  return item._id + '-' + filename;
	  }
  },
	preview_image_one: { type: Types.CloudinaryImage },
  preview_image_two: { type: Types.CloudinaryImage },
  preview_image_three: { type: Types.CloudinaryImage },
});

Project.track = true;
Project.defaultColumns = 'name, author|20%, publishedDate|20%';
Project.register();

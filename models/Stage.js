var keystone = require('keystone');
var Types = keystone.Field.Types;

var Stage = new keystone.List('Stage', {
  label: 'Stage'
});

Stage.add({
	value: { type: Number, required: true, index: true, unique: true, initial: true },
	name: { type: String, unique: false, index: false},
  education: { type: Types.Relationship, ref: 'Education', many: true, required: true, initial: true  },
  order: { type: Number, unique: false, index: false, initial: true, required: true}
});


Stage.track = true;
Stage.defaultSort = '+order';
Stage.defaultColumns = 'value, name, education, order';
Stage.register();

var keystone = require('keystone');
var Types = keystone.Field.Types;

var Education = new keystone.List('Education', {
  label: 'Education'
});

Education.add({
	value: { type: Number, required: true, index: true, unique: true, initial: true },
	name: { type: String, unique: false, index: false, required: true, initial: true},
  order: { type: Number, unique: false, index: false, required: true, initial: true}
});


Education.track = true;
Education.defaultSort = '+order';
Education.defaultColumns = 'value, name, order';
Education.register();

var keystone = require('keystone');
var Types = keystone.Field.Types;

var Module = new keystone.List('Module', {
  label: 'module'
});

Module.add({
	name: { type: String, required: true, index: true, unique: true },
	access: { type: Number, unique: false, required: true, initial: true },
  redirect: { type: String, required: true, initial: true },
  order: { type: Number, required: true, initial: true }
});


Module.track = true;
Module.defaultSort = '+order';
Module.defaultColumns = 'name, access, redirect, order';
Module.register();

import React from 'react';
import ReactDOM from 'react-dom';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import { CSSGrid, layout } from 'react-stonecutter';
import Default from './layout/Default';

const style = {
  paper: {
    textAlign: 'center',
    padding: 10,
    minHeight: 300,
    margin: 40,
    //backgroundImage: 'url(' + 'images/paper.jpeg' + ')',
  },
  paper_title: {
    fontSize: 18
  },
  loading:{
    textAlign: 'center',
    fontSize: 32
  }
}

var App = React.createClass({
  render() {
    //var dataModel = this.props.dataModel;
    //console.log(dataModel.project);
    if(this.props.dataModel){
    return(
        <div>
          <Paper style={style.paper} className="col-md-5" >
            <p style={style.paper_title}>{this.props.dataModel.project}</p>
            <RaisedButton label="button"/>
          </Paper>
          <Paper style={style.paper} className="col-md-5" >
            <p style={style.paper_title}>{this.props.dataModel.project}</p>
            <RaisedButton label="button"/>
          </Paper>
          <CSSGrid
              component="ul"
              columns={5}
              columnWidth={150}
              gutterWidth={5}
              gutterHeight={5}
              layout={layout.pinterest}
              duration={800}
              easing="ease-out">
              <li key="A" itemHeight={150}>A</li>
              <li key="B" itemHeight={120}>B</li>
              <li key="C" itemHeight={170}>C</li>
              <li key="D" itemHeight={150}>A</li>
              <li key="E" itemHeight={120}>B</li>
              <li key="F" itemHeight={170}>C</li>
              <li key="G" itemHeight={150}>A</li>
              <li key="H" itemHeight={120}>B</li>
              <li key="C" itemHeight={170}>C</li>
          </CSSGrid>
        </div>
    );
    }
    return (<div style={style.loading}>loading... </div>);
  }
});


ReactDOM.render(
  <Default><App/></Default>,
  document.getElementById('app')
);

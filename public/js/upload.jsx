import React, {Component, PropTypes}from 'react';
import ReactDOM from 'react-dom';
import DropzoneComponent from 'react-dropzone-component';
import {Tabs, Tab} from 'material-ui/Tabs';
import Default from './layout/Default';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import RichTextEditor from 'react-rte';
import FlatButton from 'material-ui/FlatButton';
import $ from 'jquery';
import {
  Step,
  Stepper,
  StepLabel,
} from 'material-ui/Stepper';

const style = {
  container: {
    textAlign: 'center',
    margin: '0 auto',
    maxWidth: '500px'
  },
  flatButton: {
    backgroundColor: "rgb(0, 188, 212)",
    color: 'white',
    margin: '8px'
  }
}

var MyStatefulEditor = React.createClass({
  propTypes: {
    onChange: PropTypes.func
  },
  getInitialState(){
    return{
      value: RichTextEditor.createEmptyValue()
    }
  },
  onChange(value){
    this.setState({value});
    if (this.props.onChange) {
      // Send the changes up to the parent component as an HTML string.
      // This is here to demonstrate using `.toString()` but in a real app it
      // would be better to avoid generating a string on each change.
      this.props.onChange(
        this.state.value.toString('html')
      );
    }
  },

  render () {
    return (
      <RichTextEditor
        value={this.state.value}
        onChange={this.onChange}
      />
    );
  }
});

var componentConfig = {
    pdf:{
      iconFiletypes: ['.pdf'],
      showFiletypeIcon: true,
      postUrl: '/uploadHandler?type=pdf'
    },
    image:{
      iconFiletypes: ['.jpg', '.png', '.jpeg'],
      showFiletypeIcon: true,
      postUrl: '/uploadHandler?type=image'
    }
};

var DropzonePdf = React.createClass({
    render: function () {
      return (
          <DropzoneComponent config={componentConfig.pdf}
                       eventHandlers={eventHandlers} />
      );
    }
});

var DropzoneImage = React.createClass({
    render: function () {
      return (
          <DropzoneComponent config={componentConfig.image}
                       eventHandlers={eventHandlers} />
      );
    }
});

var addedFileCallback = function (file) {
    console.log('I\'m a simple callback: ');
    console.log(file);
};
var eventHandlers = {
  addedfile: addedFileCallback
};

var PageContent = React.createClass({
  getInitialState(){
    return{
      outputModel: {},
    }
  },
  handleChange(jsonObj){
    this.setState({outputModel: $.extend({}, this.state.outputModel, jsonObj)});
    console.log("===current page state model===");
    console.log($.extend({}, this.state.outputModel, jsonObj));
  },
  render(){
    switch(this.props.step){
      case 0:
        return(
          <PageUpload
            handleNext={this.props.handleNext}
            lang={this.props.lang.upload}
            dataModel={this.props.dataModel}

          />
        );
        break;
      case 1:
        return(
          <PageSetup
            handlePrev={this.props.handlePrev}
            handleNext={this.props.handleNext}
            lang={this.props.lang.upload}
            dataModel={this.props.dataModel}
            onChange={this.handleChange}
            outputModel={this.state.outputModel}
          />
        );
        break;
      case 2:
        return(
          <PageInformation
            handlePrev={this.props.handlePrev}
            lang={this.props.lang.upload}
            dataModel={this.props.dataModel}
          />
        );
        break;
    }
  }
});

var PageUpload = React.createClass({
  render(){
    return(
      <div style={style.container}>
        <Tabs>
          <Tab label="Upload pdf" >
            <div>
              <DropzonePdf/>
            </div>
          </Tab>
          <Tab label="Upload photo" >
            <div>
              <DropzoneImage/>
            </div>
          </Tab>
        </Tabs>
        <br/><FlatButton style={style.flatButton} label="continue" onClick={this.props.handleNext}></FlatButton>
      </div>
    )
  }
})

var PageSetup = React.createClass({
  propTypes: {
    handleDescriptionChange: PropTypes.func,
    onChange: PropTypes.func
  },
  getInitialState(){
    return{
      title: this.props.outputModel.title || '',
      description: RichTextEditor.createValueFromString(this.props.outputModel.description || '', 'html')
    }
  },
  handleTitleChange(e){
    this.setState({title: e.target.value})
    this.props.onChange({title: e.target.value});
  },
  handleDescriptionChange(description){
    this.setState({description});
    if (this.props.onChange) {
      this.props.onChange({
        description: this.state.description.toString('html')
      });
    }
  },
  render(){
    return(
      <div style={style.container}>
        <TextField
          floatingLabelText="title"
          hintText="title"
          value={this.state.title}
          onChange={this.handleTitleChange}/>

        <RichTextEditor
          value={this.state.description}
          onChange={this.handleDescriptionChange}
        />


        <br/>
        <FlatButton label="previous" style={style.flatButton} onClick={this.props.handlePrev}></FlatButton>
        <FlatButton label="continue" style={style.flatButton} onClick={this.props.handleNext}></FlatButton>
      </div>
    );
  }
})

var PageInformation = React.createClass({
  getInitialState(){
    return{
      educations: [],
      myEduKey: [],
      myStageKey: [],
      mySchoolKey: []
    }
  },
  /*
   * event binder
   */
  handleEduSelect(e, index, value){ this.setState({myEduKey: value}); console.log('myEduKey: '+value);},
  handleStageSelect(e, index, value){ this.setState({myStageKey: value}); },
  handleSchoolSelect(e, index, value){ this.setState({mySchoolKey: value}); },
  render: function () {
    if(!this.props.dataModel)
      return (<div>loading..</div>);
    console.log(this.props.dataModel);
    var lang = this.props.lang;
    var educationObj = this.props.dataModel.education;
    var myEduKey = this.state.myEduKey;
    var myStageKey = this.state.myStageKey;
    var mySchoolKey = this.state.mySchoolKey;
    return(
      <div style={style.container}>
        <SelectField
          floatingLabelText={lang['education level']}
          value={myEduKey}
          onChange={this.handleEduSelect}>
            {Object.keys(this.props.dataModel.education).map(function(key){
              return (<MenuItem value={key} key={key} primaryText={educationObj[key].name} />)
            })}
        </SelectField>

        <SelectField
          floatingLabelText={lang['stage level']}
          value={myStageKey}
          onChange={this.handleStageSelect}>
            {
              educationObj[myEduKey] &&  educationObj[myEduKey].stage.length > 0?
              Object.keys(educationObj[myEduKey].stage).map(function(key){
                return (
                  <MenuItem
                    value={key}
                    key={key}
                    primaryText={educationObj[myEduKey].stage[key].name} />
                  )
              }) : <MenuItem primaryText={lang['no options']} disabled={true}/>
            }
        </SelectField>

        <SelectField
          floatingLabelText={lang['school']}
          value={mySchoolKey}
          onChange={this.handleSchoolSelect}>
            {
              educationObj[myEduKey] &&  educationObj[myEduKey].school.length > 0?
              Object.keys(educationObj[myEduKey].school).map(function(key){
                return (
                  <MenuItem
                    value={key}
                    key={key}
                    primaryText={educationObj[myEduKey].school[key].name} />
                  )
              }) : <MenuItem primaryText={lang['no options']} disabled={true}/>
            }
        </SelectField>
        <br/>
        <FlatButton label="previous" style={style.flatButton} onClick={this.props.handlePrev}></FlatButton>
        <FlatButton label="Finish" style={style.flatButton} onClick={this.props.finish}></FlatButton>
      </div>
    )
  }

})



var App = React.createClass({
  getInitialState(){
    return {
      stepIndex: 0,
      isLast: false,
    }
  },
  handleNext(){
    var stepIndex = this.state.stepIndex;
    this.setState({
      stepIndex: stepIndex + 1,
      isLast: stepIndex >= 1,
    });
  },
  handlePrev(){
    var stepIndex = this.state.stepIndex;
    if (stepIndex > 0) {
      this.setState({stepIndex: stepIndex - 1});
    }
  },
  render() {
    return(
        <div style={style.container}>
          <Stepper activeStep={this.state.stepIndex}>
            <Step>
              <StepLabel>Upload your notes</StepLabel>
            </Step>
            <Step>
              <StepLabel>Information</StepLabel>
            </Step>
            <Step>
              <StepLabel>Further</StepLabel>
            </Step>
          </Stepper>
          <PageContent
            step={this.state.stepIndex}
            handleNext={this.handleNext}
            handlePrev={this.handlePrev}
            dataModel={this.props.dataModel}
            lang={this.props.lang}
            />
        </div>
    );
  }
});


ReactDOM.render(<Default><App /></Default>, document.getElementById('app'));

import React from 'react';
import ReactDOM from 'react-dom';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';


const style = {
  paper: {
    textAlign: 'center',
    padding: 10,
    minHeight: 300,
    margin: 40,
    //backgroundImage: 'url(' + 'images/paper.jpeg' + ')',
  },
  paper_title: {
    fontSize: 18
  }
}

const RightIcon = () => (
  <FlatButton
    href='/auth/login'
    linkButton={true}
    secondary={true}
    label="Login"
    icon={<FontIcon className='icon-user'></FontIcon>} >
  </FlatButton>
);

var App = React.createClass({
  componentDidMount() {
    console.log(ReactDOM.findDOMNode(this).parentNode.attributes);
  },
  render() {
    return(
      <MuiThemeProvider muiTheme={getMuiTheme()}>
        <div>
          <AppBar className="app_bar" title="{title}" iconElementRight={<RightIcon></RightIcon>}/>
          <Paper style={style.paper} className="col-md-5" >
            <p style={style.paper_title}>{this.props.data.des}</p>
            <RaisedButton label="button"/>
          </Paper>
          <Paper style={style.paper} className="col-md-5" >
            <p style={style.paper_title}>{this.props.data.des}</p>
            <RaisedButton label="button"/>
          </Paper>
        </div>
      </MuiThemeProvider>
    );
  }
});

var data = {
  des: 'hello world'
};

ReactDOM.render(
  <App data={data}/>,
  document.getElementById('app')
);

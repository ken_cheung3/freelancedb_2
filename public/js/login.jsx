import React from 'react';
import ReactDOM from 'react-dom';
import RaisedButton from 'material-ui/RaisedButton';
import FontIcon from 'material-ui/FontIcon';
import Default from './layout/Default';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
//var fetch = require('fetch');


const style = {
  container: {
    margin: '0 auto',
    textAlign: 'center',
    maxWidth: '500px'
  },
  fbButton: {
    backgroundColor: '#3b5998',
    color: '#ffffff',
    label: {
      fontSize: '18px',
      textTransform: 'none',
      padding: '20px'
    }
  }
}

var App = React.createClass({
  getInitialState(){
    return{
      accountTvValue: '',
      passwordTvValue: ''
    }
  },
  _handleACTextFieldChange(e){  this.setState({accountTvValue: e.target.value}); },
  _handlePwTextFieldChange(e){  this.setState({passwordTvValue: e.target.value}); },
  _handleOnTouchSignin(){
    console.log('want to signin');
  },
  render() {
    //var dataModel = this.props.dataModel;
    //console.log(dataModel.project);
    var lang = this.props.lang;
    console.log(lang);
    if(this.props.dataModel){
    return(
        <div style={style.container}>
          <TextField value={this.state.accountTvValue} onChange={this._handleACTextFieldChange} floatingLabelText={lang.login.account_hints} hintText={lang.login.account_hints || 'account'}/>
          <TextField value={this.state.passwordTvValue} onChange={this._handlePwTextFieldChange} type="password" floatingLabelText={lang.login.password_hints} hintText={lang.login.password_hints || 'password'}/>
          <br/>
          <FlatButton label={lang.login.login_btn} onClick={this._handleOnTouchSignin}>
          </FlatButton>
          <p className="centered-divider"><span className="centered-divider-text">or</span></p>
          <RaisedButton
            label={lang.login.facebook_login}
            icon={<FontIcon className='fa fa-facebook'></FontIcon>}
            backgroundColor={style.fbButton.backgroundColor}
            labelColor={style.fbButton.color}
            labelStyle={style.fbButton.label}
            linkButton={true}
            href="/auth/facebook"
            />
        </div>
    );
    }
    return (<div>loading... </div>);
  }
});


ReactDOM.render(
  <Default><App/></Default>,
  document.getElementById('app')
);

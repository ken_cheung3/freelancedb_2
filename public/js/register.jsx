import React from 'react';
import ReactDOM from 'react-dom';
import RaisedButton from 'material-ui/RaisedButton';
import FontIcon from 'material-ui/FontIcon';
import Default from './layout/Default';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import {
  Step,
  Stepper,
  StepLabel,
} from 'material-ui/Stepper';
//var fetch = require('fetch');


const style = {
  container: {
    margin: '0 auto',
    textAlign: 'center',
    maxWidth: '500px'
  },
  fbButton: {
    backgroundColor: '#3b5998',
    color: '#ffffff',
    label: {
      fontSize: '18px',
      textTransform: 'none',
      padding: '20px'
    }
  }
}

var PageOne = React.createClass({
  getInitialState(){
    return {
      email: '',
      pw: '',
      pw2: ''
    }
  },
  _handleEMAILChange(e){  this.setState({email: e.target.value}); },
  _handlePWChange(e){  this.setState({pw: e.target.value}); },
  _handlePW2Change(e){ this.setState({pw2: e.target.value}); },
  render(){
    return(
      <div>
        <TextField value={this.state.email} hintText="Your email" onChange={this._handleEMAILChange}></TextField>
        <TextField value={this.state.pw} hintText="Your password" type="password" onChange={this._handlePWChange}></TextField>
        <TextField value={this.state.pw2} hintText="Your password again" type="password" onChange={this._handlePW2Change}></TextField>
        <br/><FlatButton label="continue" onClick={this.props.handleNext}></FlatButton>
      </div>
    )
  }
});

var PageTwo = React.createClass({
  getInitialState(){
    return {
      email: '',
      pw: '',
      pw2: ''
    }
  },
  _handleEMAILChange(e){  this.setState({email: e.target.value}); },
  _handlePWChange(e){  this.setState({pw: e.target.value}); },
  _handlePW2Change(e){ this.setState({pw2: e.target.value}); },
  render(){
    var lang = this.props.lang;
    return(
      <div>
        <SelectField floatingLabelText="School" value={this.state.value}>
          {this.props.dataModel.map(function(edu){
              return (<MenuItem value={edu.value} primaryText={lang.education[edu.value]}/>)
          })}
        </SelectField>
        <br/><FlatButton label="prev" onClick={this.props.handlePrev}>
        </FlatButton>
      </div>
    )
  }
});

var PageContent = React.createClass({
  render(){
    switch(this.props.step){
      case 0:
        return (<PageOne handleNext={this.props.handleNext}/>);
        break;
      case 1:
        return (
          <PageTwo
            handlePrev={this.props.handlePrev}
            lang={this.props.lang.register}
            dataModel={this.props.dataModel}
            />);
        break;
    }
  }
})



var App = React.createClass({
  getInitialState(){
    return {
      stepIndex: 0,
      isLast: false,
    }
  },
  handleNext(){
    var stepIndex = this.state.stepIndex;
    this.setState({
      stepIndex: stepIndex + 1,
      isLast: stepIndex >= 1,
    });
  },
  handlePrev(){
    var stepIndex = this.state.stepIndex;
    if (stepIndex > 0) {
      this.setState({stepIndex: stepIndex - 1});
    }
  },
  render() {
    return(
        <div style={style.container}>
          <Stepper activeStep={this.state.stepIndex}>
            <Step>
              <StepLabel>Basic Information</StepLabel>
            </Step>
            <Step>
              <StepLabel>More About you</StepLabel>
            </Step>
          </Stepper>
          <PageContent
            step={this.state.stepIndex}
            handleNext={this.handleNext}
            handlePrev={this.handlePrev}
            dataModel={this.props.dataModel}
            lang={this.props.lang}
            />
        </div>
    );
  }
});


ReactDOM.render(
  <Default><App/></Default>,
  document.getElementById('app')
);

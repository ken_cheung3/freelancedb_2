import React from 'react';
import ReactDOM from 'react-dom';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import MenuItem from 'material-ui/MenuItem';
import Menu from 'material-ui/Menu';
import Drawer from 'material-ui/Drawer';
import Avatar from 'material-ui/Avatar';
import injectTapEventPlugin from 'react-tap-event-plugin';
import ArrowDropRight from 'material-ui/svg-icons/navigation-arrow-drop-right';
import $ from 'jquery';

injectTapEventPlugin();

const style = {
  avatar: {
    verticalAlign: 'middle'
  }
}

const LoginIcon = () => (
  <FlatButton
    href='/auth/login'
    linkButton={true}
    secondary={true}
    label="Login"
    icon={<FontIcon className='fa fa-user'></FontIcon>}>
  </FlatButton>
);

const LogoutIcon = () => (
  <FlatButton
    href='/keystone/signout'
    linkButton={true}
    secondary={true}
    label="Logout"
    icon={<FontIcon className='fa fa-sign-out'></FontIcon>}>
  </FlatButton>
)

function _onHandleLangClick(e){
  $.post('/locale', {locale: this}, function(data){
    window.location.reload();
    console.log(data);
  });
}

var App = React.createClass({
  getInitialState(){
      return {
        modules: [],
        lang: {title: 'HomeworkNET'},
        userModel: {},
        drawerOpen: false
      }
  },
  componentDidMount(){
    // will this be bottleneck?
    var realDOMAttr = ReactDOM.findDOMNode(this).parentNode.attributes;
    var lang = JSON.parse(realDOMAttr['lang'].value);
    var dataModel = JSON.parse(realDOMAttr['data-model'].value);
    var modules = JSON.parse(realDOMAttr['modules'].value);
    var userModel = JSON.parse(realDOMAttr['user-model'].value);
    var i18nLocale = JSON.parse(realDOMAttr['i18nLocale'].value);
    var i18nCatalog = JSON.parse(realDOMAttr['i18nCatalog'].value);
    console.log(Object.keys(i18nCatalog));
    this.setState({lang: lang});
    this.setState({dataModel: dataModel});
    this.setState({modules: modules});
    this.setState({userModel: userModel});
    this.setState({i18nLocale: i18nLocale});
    this.setState({i18nCatalog: i18nCatalog});
  },
  _handleTouchDrawerLeft(){
    this.setState({drawerOpen: !this.state.drawerOpen});
  },
  _onRequestDrawerChange(open,reason){
    if(!open && reason == 'clickaway'){
      this.setState({drawerOpen: false});
    }
  },
  _onClickMenu(e,item,index){
    console.log(index);
  },
  render() {
    const lang = this.state.lang;
    const _this = this;
    return(
      <MuiThemeProvider muiTheme={getMuiTheme()}>
        <div>
          <AppBar
            className="app_bar"
            title={lang.title}
            onLeftIconButtonTouchTap={this._handleTouchDrawerLeft}
            iconElementRight={this.state.userModel._id? <div><Avatar style={style.avatar} src={this.state.userModel.facebookIcon}/><LogoutIcon></LogoutIcon></div> : <LoginIcon></LoginIcon>}
            />
          <Drawer docked={false} open={this.state.drawerOpen} onRequestChange={this._onRequestDrawerChange}>
            <Menu onItemTouchTap={this._onClickMenu}>
              {this.state.modules.map(function(module){
                return (<MenuItem key={module.name}>{lang.module_name[module.name]}</MenuItem>)
              })}
              <MenuItem
                primaryText={lang['language']}
                rightIcon={<ArrowDropRight />}
                menuItems={
                  this.state.i18nCatalog ? this.state.i18nCatalog.map(function(locale){
                    return (
                      <MenuItem
                        key={locale}
                        value={locale}
                        primaryText={lang['name-'+locale]}
                        onTouchTap={(e)=>_onHandleLangClick.call(locale, e)}
                        />
                    )
                  }):''}/>
            </Menu>
          </Drawer>
          {React.cloneElement(this.props.children, { dataModel: this.state.dataModel, lang: lang})}
        </div>
      </MuiThemeProvider>
    );
  }
});

export default App;
